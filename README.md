# fake-smtp-server-dockerfile

Fake SMTP Server in a Docker (DEV env only)

Based on: https://www.npmjs.com/package/fake-smtp-server

Ports exposes in Dockerfile:
- 1025 for SMTP server
- 1080 for HTTP server

Add it in a docker-compose:
`
version: "2"
services:
    fakesmtp:
        build:
        context: .
        dockerfile: Dockerfile-smtp
        ports:
        - "1025:1025"
        - "1080:1080"
`
